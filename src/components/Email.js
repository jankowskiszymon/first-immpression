import React from 'react';
import styled from 'styled-components';

const EmailDiv = styled.div`
    font-size: 9vh;
    margin-top: 50px;
    color: black;
    pointer-events: auto;
    text-decoration: none;
    a{
        text-decoration: none;
        color: black;
    }
`   
const Border1 = styled.div`
    height: 50px;
    width: 4.5px;
    background: black;
    position: relative;
    margin-left: 43px;
    margin-top: -2px;
`
export const BorderBottom = styled.div`
    height: ${props => props.height ? props.height : '0px'};
    width: ${props => props.width ? props.width : '0px'};
    margin-top: ${props => props.marginTop ? props.marginTop : '0px'};
    margin-left: ${props => props.marginLeft ? props.marginLeft : '0px'};
    position: relative;
    border: 0.5px solid black;
    border-top: white;
    z-index: 10;
    pointer-events: auto;
    font-size: 3vh;
    
`

export const BorderTop = styled.div`
    height: ${props => props.height ? props.height : '0px'};
    width: ${props => props.width ? props.width : '0px'};
    margin-top: ${props => props.marginTop ? props.marginTop : '0px'};
    margin-left: ${props => props.marginLeft ? props.marginLeft : '0px'};
    position: relative;
    border: 1px solid black;
    border-bottom: white;
    pointer-events: auto;
    font-size: 3vh;
`

const Email = () => {

    return (
        <EmailDiv>
            Szymon@<a href="https://jankowskiszymon.pl">JankowskiSzymon.pl</a>
        </EmailDiv>

    )
}

export default Email;