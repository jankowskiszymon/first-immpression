import Parallax from 'parallax-js';
import React, { useEffect } from 'react';
import styled from 'styled-components';
import Email, { Border, BorderBottom, BorderTop } from './Email';
import Portfolio from './Portfolio';


const Bg = styled.div`

`

const Scene = styled.div`
    width: 70vw;
    height:100px;
    margin: 0 auto;
    text-align: center;
    pointer-events: auto;
    
    
`

const PortfolioDiv = styled.div`
    margin-top: 300px;
    display: flexbox;
    height: 300px;
    width: 100%;
    pointer-events: auto;
    border: 1px solid black;
    `

const Background = () => {

    useEffect(()=>{
        var scene = document.getElementById('scene');
        console.log(scene);
        // eslint-disable-next-line
        var parallaxInstance = new Parallax(scene);
    }, [])

    return (
        <Scene id="scene">
            <Bg data-depth="0.1">
                <Email/>
            </Bg>
            <Bg data-depth="0.3">
                <BorderBottom 
                    height="4.5vh"
                    width="63vw"
                    marginTop="18vh"
                    marginLeft="3.7vw"
                    >
                contact         
                </BorderBottom>
            </Bg>

            <Bg data-depth="0.05">
                <BorderTop
                    height="4.5vh"
                    width="530px"
                    marginTop="-20px"
                    marginLeft="311px">
                Website
                </BorderTop>
            </Bg>
            <Bg data-depth="0.2">
                <BorderBottom 
                    height="4.4vh"
                    width="40vw"
                    marginTop="12vh"
                    marginLeft="21vw">
                    <a href="https://gitlab.com/jankowskiszymon">Gitlab</a> | <a href="https://www.linkedin.com/in/jankowskiszymon/" >Linkedin</a> | <a href="">Facebook</a>
                </BorderBottom>
            </Bg>

            <PortfolioDiv data-depth="0.1">
                <Portfolio />
            </PortfolioDiv>

        </Scene>

    )
}

export default Background;